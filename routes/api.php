<?php

use App\Events\TestLaravelEchoEvent;
use App\Events\TestLaravelEchoPrivateEvent;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\Api\PostController;
use App\Http\Controllers\Api\CommonController;
use App\Http\Controllers\API\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);

Route::group(['middleware' => 'auth:api'], function () {



























    Route::get('event-test', function () {
        try {
            broadcast(new TestLaravelEchoEvent());
            broadcast(new TestLaravelEchoPrivateEvent(1));
            return ['status' => true, 'message' => 'broadcast success'];
        } catch (\Throwable $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    });























    //post
    Route::resource('/post', PostController::class);
    Route::post('/comment', [PostController::class, 'postComment']);
    Route::get('/comment', [PostController::class, 'fetchComment']);
    Route::post('/like-unlike/{type}/{post_id}', [PostController::class, 'postLike']);
    Route::get('/likes/{type}/{post_id}', [PostController::class, 'fetchLike']);
    Route::post('/save-unsave/{post_id}', [PostController::class, 'saveUnsavePost']);


    //profile
    Route::resource('/user', UserController::class);
    Route::post('/user/{id}/profile', [UserController::class, 'update']);
    Route::post('/user/follow-unfollow', [UserController::class, 'followUnfollowUser']);
    Route::post('/user/profile-image', [UserController::class, 'profileImageUpload']);
    Route::get('/profile/{user_type}/{friendId?}', [UserController::class, 'getFollowerOrFollowing'])->where('user_type', '[A-Za-z]+');

    //media
    Route::post('/media', [CommonController::class, 'postTmpUpload']);
    Route::delete('/media', [CommonController::class, 'uploadTmpDelete']);
});
