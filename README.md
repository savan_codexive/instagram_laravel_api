php artisan make:event TestLaravelEchoEvent

<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class TestLaravelEchoEvent implements ShouldBroadcastNow
{
    public function __construct()
    {
    }

    public function broadcastOn()
    {
        return new Channel('test-channel');
    }

    public function broadcastAs()
    {
        return 'test-listener';
    }

    public function broadcastWith()
    {
        return [
            'hello' => "this is travooo port testing"
        ];
    }
}

------------------------------------------------------------



class BroadcastServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if (request()->hasHeader('authorization')) {
            Broadcast::routes(['middleware' => 'auth:api']);
        } else {
            Broadcast::routes();
        }
 	require base_path('routes/channels.php');
    }
}


------------------------------------------------------------


Route::get('event-test', function () {
    try {
        broadcast(new TestLaravelEchoEvent());
        return ['status' => true, 'message' => 'broadcast success'];
    } catch (\Throwable $e) {
        return ['status' => false, 'message' => $e->getMessage()];
    }
});



------------------------------------------------------------
1) composer require predis/predis
2) .env

BROADCAST_DRIVER=redis
CACHE_DRIVER=file
QUEUE_CONNECTION=sync
QUEUE_DRIVER=redis

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

php artisan optimize

------------------------------------------------------------


$ npm install -g laravel-echo-server

$ laravel-echo-server init

? Do you want to run this server in development mode? Yes
? Which port would you like to serve from? 6001
? Which database would you like to use to store presence channel members? redis
? Enter the host of your Laravel authentication server. http://127.0.0.1/
? Will you be serving on http or https? http
? Do you want to generate a client ID/Key for HTTP API? No
? Do you want to setup cross domain access to the API? No
? What do you want this config to be saved as? laravel-echo-server.json
Configuration file saved. Run laravel-echo-server start to run server.

laravel-echo-server.json
{
    "authHost": "http://127.0.0.1:8000",
    "authEndpoint": "/broadcasting/auth",
    "clients": [],
    "database": "redis",
    "databaseConfig": {
        "redis": {
            "port": "6379",
            "host": "127.0.0.1"
        },
        "sqlite": {
            "databasePath": "/database/laravel-echo-server.sqlite"
        }
    },
    "devMode": true,
    "host": null,
    "port": "6001",
    "protocol": "http",
    "socketio": {},
    "secureOptions": 67108864,
    "sslCertPath": "",
    "sslKeyPath": "",
    "sslCertChainPath": "",
    "sslPassphrase": "",
    "subscribers": {
        "http": true,
        "redis": true
    },
    "apiOriginAllow": {
        "allowCors": false,
        "allowOrigin": "",
        "allowMethods": "",
        "allowHeaders": ""
    }
}


$ laravel-echo-server start

L A R A V E L  E C H O  S E R V E R

version 1.6.2

⚠ Starting server in DEV mode...

✔  Running at localhost on port 6001
✔  Channels are ready.
✔  Listening for http events...
✔  Listening for redis events...

Server ready!

