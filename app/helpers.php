<?php

use App\Models\ActivityLog;
use App\Models\PostLike;
use App\Models\PostShare;
use App\Models\UsersMedias;

defined('PHOTO_PLACEHOLDERS')    or define('PHOTO_PLACEHOLDERS', 'default/images/photo.png');
defined('MALE_PLACEHOLDERS')    or define('MALE_PLACEHOLDERS', 'default/images/male.png');
defined('FEMALE_PLACEHOLDERS')    or define('FEMALE_PLACEHOLDERS', 'default/images/female.png');
defined('PATTERN_PLACEHOLDERS') or define('PATTERN_PLACEHOLDERS', 'default/images/pattern.png');
defined('HOTEL_NO_PHOTO')       or define('HOTEL_NO_PHOTO', 'default/images/no-photo.png');

defined('TYPE_POST')        or define('TYPE_POST', 'post');
defined('TYPE_MEDIA')       or define('TYPE_MEDIA', 'media');


if (!function_exists('_getDBQuery')) {

    function _getDBQuery($query)
    {
        $bindings = $query->getBindings();
        $sql = str_replace('?', '%s', $query->toSql());
        return sprintf($sql, ...$bindings);
    }
}

if (!function_exists('saveActivityLog')) {
    function saveActivityLog($type, $action, $val, $userId)
    {
        $log = new ActivityLog;
        $log->type = $type;
        $log->action = $action;
        $log->val = $val;
        $log->user_id  = $userId;
        $log->save();
        return $log;
    }
}

if (!function_exists('savePostLike')) {
    function savePostLike($postId, $userId, $type)
    {
        $like = new PostLike;
        $like->user_id = $userId;
        $like->post_id = $postId;
        $like->type = $type;
        $like->save();
        return $like;
    }
}

if (!function_exists('getTempFiles')) {
    function getTempFiles($pair)
    {
        $pair = ($pair) ? $pair : -1;
        $user = Auth::user();
        $temp_dir = public_path() . '/assets/upload_tmp/';
        $fileLists = [];

        if (is_dir($temp_dir)) {
            if ($handle = opendir($temp_dir)) {
                while (false !== ($file = readdir($handle))) {
                    if ($file == '.' || $file == '..') {
                        continue;
                    }
                    if (strpos(($temp_dir . $file), $pair . '_' . $user->id) !== false) {
                        $filename_split = explode($pair . '_' . $user->id . '_', $file, 2);
                        $fileLists[] = [$temp_dir . $file, $filename_split[1]];
                    }
                }
                closedir($handle);
            }
        }

        return $fileLists;
    }
}

if (!function_exists('userProfilePicture')) {
    function userProfilePicture($user)
    {
        $url = null;
        $userMedia = UsersMedias::query()->whereHas('media')->with('media')->where('id', $user->profile_picture)->first();
        if ($userMedia && $userMedia->media) {
            $url = $userMedia->media->url;
        }
        return getMediaImageUrl($url, $user->gender);
    }
}


if (!function_exists('getMediaImageUrl')) {
    function getMediaImageUrl($url, $gender = '')
    {
        if (!$url) {
            switch ($gender) {
                case 'male':
                    return url(MALE_PLACEHOLDERS);

                case 'female':
                    return url(FEMALE_PLACEHOLDERS);

                default:
                    return url(PHOTO_PLACEHOLDERS);
            }
        }
        return url('storage/' . $url);
    }
}
