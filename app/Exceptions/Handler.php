<?php

namespace App\Exceptions;

use App\Http\Responses\ApiResponse;
use ErrorException;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Session\TokenMismatchException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;

class Handler extends ExceptionHandler
{
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    public function report(Throwable $e)
    {
        parent::report($e);
    }

    public function render($request, Throwable $e)
    {
        if (strpos(url()->current(), '/api')) {
            if (($e instanceof TokenMismatchException) || ($e instanceof AuthenticationException)) {
                return ApiResponse::__createUnAuthorizedResponse();
            } else if ($e instanceof ErrorException) {
                return ApiResponse::__createServerError($e->getMessage());
            } else if ($e instanceof NotFoundHttpException) {
                return ApiResponse::create(["message" => ["Page Not Found"]], false, ApiResponse::NOT_FOUND);
            } else if ($e instanceof MethodNotAllowedException) {
                return ApiResponse::__createServerError("invalid api request method");
            } else if ($e instanceof MethodNotAllowedHttpException) {
                return ApiResponse::__createServerError("Api Request method is invalid");
            }
            return ApiResponse::createServerError($e);
        }
        return parent::render($request, $e);
    }

    public function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson() or strpos(url()->current(), '/api')) {
            return ApiResponse::__createUnAuthorizedResponse();
        }
        return redirect()->guest('/login');
    }
}
