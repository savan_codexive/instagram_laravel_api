<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserSave extends Model
{
    use HasFactory;

    protected $table = 'user_save';
    protected $guarded = [];

    public function activityLogs()
    {
        return $this->belongsTo(ActivityLog::class, 'activity_log_id');
    }
}
