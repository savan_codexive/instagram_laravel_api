<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActivityLog extends Model
{
    use HasFactory, SoftDeletes;

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    // post
    const TYPE_POST = 'post';
    const ACTION_POST_PUBLISH   = 'publish';
    const ACTION_POST_COMMENT   = 'comment';
    const ACTION_POST_LIKE      = 'like';
    const ACTION_POST_UNLIKE    = 'unlike';
    const ACTION_POST_SAVE    = 'save';

    //EDIT
    const TYPE_USER = 'user';
    const ACTION_PROFILE_UPDATE    = 'profile_update';
}
