<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostsMedias extends Model
{
    use HasFactory;
    public $timestamps = false;

    public function post()
    {
        return $this->hasOne(Post::class, 'id', 'post_id');
    }

    public function media()
    {
        return $this->hasOne(Media::class, 'id', 'media_id');
    }
}
