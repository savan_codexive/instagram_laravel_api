<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function likes()
    {
        return $this->hasMany(PostLike::class, 'post_id')->where('type', TYPE_MEDIA)->orderBy('created_at', 'DESC');
    }

    public function comment()
    {
        return $this->hasMany(PostComment::class, 'post_id')->where('type', TYPE_MEDIA)->orderBy('created_at', 'DESC');
    }

}
