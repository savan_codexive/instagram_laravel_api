<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use HasFactory, SoftDeletes;

    const ACCESS_PUBLIC = 1;
    const ACCESS_PRIVATE = 2;

    const ACCESS_MAPPING = [
        self::ACCESS_PUBLIC  => "public",
        self::ACCESS_PRIVATE  => "private",
    ];

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function likes()
    {
        return $this->hasMany(PostLike::class, 'post_id')->where('type', TYPE_POST)->orderBy('created_at', 'DESC');
    }

    public function comment()
    {
        return $this->hasMany(PostComment::class, 'post_id')->where('type', TYPE_POST)->orderBy('created_at', 'DESC');
    }

    public function medias()
    {
        return $this->hasMany(PostsMedias::class,  'post_id', 'id');
    }
}
