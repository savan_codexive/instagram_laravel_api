<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UsersMedias extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $guarded = [];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function media()
    {
        return $this->hasOne(Media::class, 'id', 'media_id');
    }
}
