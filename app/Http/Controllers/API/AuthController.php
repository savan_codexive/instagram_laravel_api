<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\SignInRequest;
use App\Http\Requests\Api\SignUpRequest;
use App\Http\Responses\ApiResponse;
use App\Models\User;

class AuthController extends Controller
{
    /**
     * @param SignUpRequest $request
     * @return  ApiResponse::create
     */
    function register(SignUpRequest $request)
    {
        try {
            $validatedData = $request->all();
            $validatedData['password'] = bcrypt($request->password);
            $user = User::create($validatedData);
            $accessToken = $user->createToken('authToken')->accessToken;
            $user->profile_picture = $user->getProfilePicture();
            return ApiResponse::create(['user' => $user, 'access_token' => $accessToken]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * @param SignInRequest $request
     * @return  ApiResponse::create
     */
    function login(SignInRequest $request)
    {
        try {
            $loginData = $request->only('email', 'password');
            if (!auth()->attempt($loginData))  return ApiResponse::__createBadResponse('Invalid Credentials');
            $accessToken = auth()->user()->createToken('authToken')->accessToken;
            $user = auth()->user();
            $user->profile_picture = $user->getProfilePicture();
            return ApiResponse::create(['user' => $user, 'access_token' => $accessToken]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }
}
