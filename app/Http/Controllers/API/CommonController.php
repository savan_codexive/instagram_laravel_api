<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Responses\ApiResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CommonController extends Controller
{
    public function postTmpUpload(Request $request)
    {
        try {
            $user       = Auth::user();
            $validator  = Validator::make($request->all(), [
                'file'        => 'required',
                'pair'        => 'required'
            ]);
            if ($validator->fails()) {
                return ApiResponse::create($validator->errors(), false, ApiResponse::VALIDATION);
            } else {
                $validator = Validator::make($request->all(), [
                    'file'    => 'mimes:jpeg,jpg,png,mp4,mov,ogg,wmv,heic,gif,tiff'
                ], [
                    'file.mimes' => 'only allow .jpeg, .jpg, .png, .mp4, .mov, .ogg, .wmv, .heic, .gif, .tiff media'
                ]);

                if ($validator->fails()) {
                    return ApiResponse::create($validator->errors(), false, ApiResponse::VALIDATION);
                }
            }
            $_base_path = '/assets/upload_tmp';
            if ($request->hasFile('file')) {
                $file               = $request->file('file');
                $pair               = $request->pair ?? '';
                $fileOriginalName   = $file->getClientOriginalName();
                $filename           = $pair . '_' . $user->id . '_' . $fileOriginalName;
                $path               = public_path($_base_path) . '/';
                @chmod($path, 0777);
                if ($file->move($path, $filename)) {
                    return ApiResponse::create([
                        "message"   => "upload successfully",
                        "filename"  => $fileOriginalName,
                        'url' => url($_base_path) . "/" . $filename
                    ]);
                } else {
                    return ApiResponse::__createBadResponse("failed to upload media");
                }
            } else {
                return ApiResponse::__createBadResponse("something went wrong in file");
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    public function uploadTmpDelete(Request $request)
    {
        try {
            $user       = Auth::user();
            $validator  = Validator::make($request->all(), [
                'pair'          => 'required',
                'file_name'     => 'required',
            ]);
            if ($validator->fails()) {
                return ApiResponse::create($validator->errors(), false, ApiResponse::VALIDATION);
            }

            $filename           = $request->file_name   ?? '';
            $pair               = $request->pair        ?? '';
            $filename           = $pair . '_' . $user->id . '_' . $filename;
            $filepath           = public_path() . '/assets/upload_tmp/' . $filename;
            if (file_exists($filepath)) {
                unlink($filepath);
                return ApiResponse::__create("delete successfully");
            } else {
                return ApiResponse::__createBadResponse("file not found.");
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }
}
