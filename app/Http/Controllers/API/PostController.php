<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Responses\ApiResponse;
use App\Models\User;
use App\Models\Post;
use App\Models\Media;
use App\Models\PostsMedias;
use App\Models\UsersMedias;
use App\Models\PostComment;
use App\Models\ActivityLog;
use App\Models\PostLike;
use App\Models\UserSave;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $page               = (int) request('page', 1);
            $per_page           = (int) request('per_page', 10);
            $skip               = ($page - 1) * $per_page;

            $baseQuery = ActivityLog::query()
                ->where(function ($query) {
                    $query->where('type', ActivityLog::TYPE_POST);
                    $query->where(function ($q) {
                        $q->whereIn('action', [
                            ActivityLog::ACTION_POST_PUBLISH,
                        ]);
                    });
                });

            $total = (clone $baseQuery)->count();
            $logs  = (clone $baseQuery)->orderBy('created_at', 'desc')->skip($skip)->take($per_page)->get();

            $posts = [];
            $logs->each(function ($log) use (&$posts) {
                switch ($log->type) {
                    case ActivityLog::TYPE_POST:
                        if ($log->action == ActivityLog::ACTION_POST_PUBLISH) {
                            $post = Post::query()->with(['author'])->where('id', $log->val)->first();
                            if ($post) {
                                $post->like_flag    = $post->likes()->where('user_id', auth()->id())->exists();
                                $post->total_likes  = $post->likes()->count();
                                $post->comments     = $post->comment()->with(['author'])->take(5)->get();
                                $log->data = $post;
                                $posts[] = $log;
                            }
                        }
                        break;
                }
            });

            $data['total_page'] = ceil($total / $per_page);
            $data['current_page'] = $page;
            $data['per_page'] = $per_page;
            $data['data'] = $posts;
            return ApiResponse::create($data);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $authUser = auth()->user();
            $user_id  = $authUser->id;
            $fileLists = getTempFiles($request->input('pair'));

            $newPost = new Post;
            $newPost->text = $request->text;
            $newPost->access = $request->access;
            $newPost->user_id = $user_id;
            $newPost->save();

            foreach ($fileLists as $file) {
                $filename = $authUser->id . '_' . time() . '_' . $file[1];
                Storage::disk()->put('post-photo/' . $filename, fopen($file[0], 'r+'), 'public');
                $path = url('storage/post-photo') . '/' . $filename;
                @unlink($file[0]);

                $media              = new Media();
                $media->url         = $path;
                $media->user_id     = $authUser->id;
                $media->save();

                $users_medias           = new UsersMedias();
                $users_medias->user_id  = $authUser->id;
                $users_medias->media_id = $media->id;
                $users_medias->save();

                $posts_medias           = new PostsMedias();
                $posts_medias->post_id  = $newPost->id;
                $posts_medias->media_id = $media->id;
                $posts_medias->save();
            }


            $log = saveActivityLog(ActivityLog::TYPE_POST, ActivityLog::ACTION_POST_PUBLISH, $newPost->id, $user_id);

            $post = Post::query()->with(['author'])->where('id', $log->val)->first();
            $post->like_flag    = $post->likes()->where('user_id', $user_id)->exists();
            $post->total_likes  = $post->likes()->count();
            $post->comments     = $post->comment()->with(['author'])->take(5)->get();

            $post->medias->each(function ($postMedia) {
                $postMedia->media;
            });

            $log->data = $post;

            return ApiResponse::create($log);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    // comment section
    public function postComment(Request $request)
    {
        try {
            $authUser   = auth()->user();
            $type       = $request->type;
            $post_id    = $request->post_id;
            $text       = $request->text;

            if (in_array($type, [TYPE_POST, TYPE_MEDIA])) {

                switch ($type) {
                    case TYPE_POST:
                        $obj = Post::find($post_id);
                        break;
                    case TYPE_MEDIA:
                        $obj = Media::find($post_id);
                        break;
                }

                if (!$obj) {
                    return ApiResponse::createValidationResponse([
                        'post_id' => $type . ' not found'
                    ]);
                }

                $comment = new PostComment;
                $comment->type = $type;
                $comment->user_id = $authUser->id;
                $comment->post_id = $obj->id;
                $comment->text = $text;
                $comment->save();
            } else {
                return ApiResponse::createValidationResponse([
                    'type' => 'type is invalid'
                ]);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    function fetchComment(Request $request)
    {
        try {
            $authUser   = auth()->user();
            $type       = $request->type;
            $post_id    = $request->post_id;

            if (in_array($type, [TYPE_POST, TYPE_MEDIA])) {
                switch ($type) {
                    case TYPE_POST:
                        $obj = Post::find($post_id);
                        break;
                    case TYPE_MEDIA:
                        $obj = Media::find($post_id);
                        break;
                }

                if (!$obj) {
                    return ApiResponse::createValidationResponse([
                        'post_id' => $type . ' not found'
                    ]);
                }

                $comments = PostComment::query()
                    ->with('author')
                    ->where('type', $type)
                    ->where('post_id', $obj->id)
                    ->paginate($request->input('per_page', 10));

                return ApiResponse::create($comments);
            } else {
                return ApiResponse::createValidationResponse([
                    'type' => 'type is invalid'
                ]);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }


    function postLike(Request $request, $type, $post_id)
    {
        try {
            $authUser   = auth()->user();
            $type       = strtolower(trim($type));

            if (in_array($type, [TYPE_POST, TYPE_MEDIA])) {
                switch ($type) {
                    case TYPE_POST:
                        $obj = Post::find($post_id);
                        break;
                    case TYPE_MEDIA:
                        $obj = Media::find($post_id);
                        break;
                }

                if (!$obj) {
                    return ApiResponse::createValidationResponse([
                        'post_id' => $type . ' not found'
                    ]);
                }

                $baseQuery = PostLike::query()->where(['type' => $type, 'post_id' => $obj->id]);
                if ((clone $baseQuery)->where(['user_id' => $authUser->id])->exists()) {
                    (clone $baseQuery)->where(['user_id' => $authUser->id])->delete();
                    $res['like_flag'] = false;
                } else {
                    $newLike = new PostLike;
                    $newLike->type = $type;
                    $newLike->user_id = $authUser->id;
                    $newLike->post_id = $obj->id;
                    $newLike->save();
                    $res['like_flag'] = true;
                }
                saveActivityLog($type, $res['like_flag'] ? ActivityLog::ACTION_POST_LIKE : ActivityLog::ACTION_POST_UNLIKE, $obj->id, $authUser->id);
                $res['total_likes'] = (clone $baseQuery)->count();
                return ApiResponse::create($res);
            } else {
                return ApiResponse::createValidationResponse([
                    'type' => 'type is invalid'
                ]);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    function fetchLike(Request $request, $type, $post_id)
    {
        try {
            $type       = strtolower(trim($type));

            if (in_array($type, [TYPE_POST, TYPE_MEDIA])) {
                switch ($type) {
                    case TYPE_POST:
                        $obj = Post::find($post_id);
                        break;
                    case TYPE_MEDIA:
                        $obj = Media::find($post_id);
                        break;
                }

                if (!$obj) {
                    return ApiResponse::createValidationResponse([
                        'post_id' => $type . ' not found'
                    ]);
                }

                $likes = $obj->likes()->with('author')->paginate($request->input('per_page', 10));
                return ApiResponse::create($likes);
            } else {
                return ApiResponse::createValidationResponse([
                    'type' => 'type is invalid'
                ]);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    function saveUnsavePost($post_id ,Request $request)
    {
        try{
            $userId = $request->user_id;
            $savedPost = UserSave::where(['user_id' => $userId])->whereHas('activityLogs', function($q) use ($post_id) {
                return $q->where('val', $post_id)->where('action', ActivityLog::ACTION_POST_SAVE);
            })->first();

            if ($savedPost) {
                $savedPost->delete();
                return ApiResponse::create([
                    'message' => 'Post unsaved',
                    'save_flag' => false
                ]);
            }

            $activityLog = saveActivityLog(ActivityLog::TYPE_POST, ActivityLog::ACTION_POST_SAVE, $post_id, $userId);
            UserSave::create([
                'user_id' => $userId,
                'activity_log_id' => $activityLog->id
            ]);

            return ApiResponse::create([
                'message' => 'Post saved',
                'save_flag' => true
            ]);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }
}
