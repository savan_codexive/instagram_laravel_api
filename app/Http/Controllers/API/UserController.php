<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Responses\ApiResponse;
use App\Models\ActivityLog;
use App\Models\Media;
use App\Models\User;
use App\Models\UserFriend;
use App\Models\UsersMedias;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class UserController extends Controller
{
    public function index()
    {
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        try {
            if ($request->hasFile('profile_picture')) {
                $userMedia = $this->profileImageUpload($request, true);
            }
            $attributes = $request->only('username', 'phone_no', 'dob', 'gender');
            if (isset($attributes['dob']) && $attributes['dob'] != "") {
                $attributes['dob'] = Carbon::parse($attributes['dob']);
            }

            $user = User::find($id);
            $user->update($attributes);
            $user->profile_picture = $user->getProfilePicture();
            return ApiResponse::create($user);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    public function destroy($id)
    {
        //
    }

    public function profileImageUpload(Request $request, $flag = false)
    {
        try {
            $authUser = auth()->user();
            $image = $request->file('profile_picture');
            $newMediaPath = Storage::disk()->put('media/profile', $image);

            $media = Media::create([
                'user_id'   =>  $authUser->id,
                'url'       =>  $newMediaPath
            ]);

            $userMedia =  UsersMedias::create([
                'user_id'   => $authUser->id,
                'media_id'  =>  $media->id
            ]);

            saveActivityLog(ActivityLog::TYPE_USER, ActivityLog::ACTION_PROFILE_UPDATE, $userMedia->id, $authUser->id);

            $authUser->profile_picture = $userMedia->id;
            $authUser->save();
            if ($flag) {
                return $userMedia;
            }
            $authUser->profile_picture = $authUser->getProfilePicture();
            return ApiResponse::create($authUser);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    //user_id = follow karva valo + friend_id = jene follow karvo hoy e
    public function followUnfollowUser(Request $request)
    {
        try {
            $userFriend = UserFriend::where('user_id', auth()->user()->id)->where('friend_id', $request->user_id)->first();
            if ($userFriend) {
                $userFriend->delete();

                return ApiResponse::create([
                    'message' => "you're unfollowed this user",
                    'is_following'  => false
                ]);
            } else {
                UserFriend::create([
                    'user_id'   =>  auth()->user()->id,
                    'friend_id' =>  $request->user_id
                ]);

                return ApiResponse::create([
                    'message' => "you're following this user",
                    'is_following'  => true
                ]);
            }
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }

    // get user follower and follwing api
    public function getFollowerOrFollowing(Request $request, $user_type, $friendId = null)
    {
        try {
            $authUser = auth()->user();
            $_id = ($friendId) ? $friendId : $authUser->id;
            $users = UserFriend::when($user_type == "following", function ($q) use ($_id) {
                $q->where('user_id', $_id)->with('friend');
            }, function ($q) use ($_id) {
                $q->where('friend_id', $_id)->with('user');
            })->paginate($request->input('per_page', 10));

            $lst = $users->getCollection()->map(function ($userFriend) use ($authUser, $user_type) {
                $userFriend->follow_flag = UserFriend::where('user_id', $authUser->id)->where('friend_id', ($user_type == "following") ? $userFriend->friend_id : $userFriend->user_id)->exists();
                if ($user_type == "follower") {
                    $userFriend->friend = $userFriend->user;
                    unset($userFriend->user);
                }
                $userFriend->friend->profile_picture = $userFriend->friend->getProfilePicture();
                return $userFriend;
            });
            $users->setCollection($lst);
            return ApiResponse::create($users);
        } catch (\Throwable $e) {
            return ApiResponse::createServerError($e);
        }
    }
}
