<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Api\BaseRequest;

class SignUpRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'name' => 'required|max:55',
            'email' => 'email|required|unique:users',
            'password' => 'required|confirmed'
        ];
    }
}
