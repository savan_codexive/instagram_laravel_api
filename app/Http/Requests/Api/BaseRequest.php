<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Responses\ApiResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;

class BaseRequest extends FormRequest
{
    public function onlyValidated()
    {
        return $this->only(array_keys($this->rules()));
    }

    function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json([
                'data'   => null,
                'error' => $errors,
                'status' => ApiResponse::VALIDATION,
                'success' => false
            ], ApiResponse::VALIDATION)
        );
    }
}
