<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Api\BaseRequest;

class SignInRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'email' => 'email|required',
            'password' => 'required'
        ];
    }
}
