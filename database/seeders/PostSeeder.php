<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\User;
use App\Models\Post;
use App\Models\PostComment;
use App\Models\PostShare;
use App\Models\PostLike;
use App\Models\ActivityLog;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        User::inRandomOrder()->take(rand(5, 10))->pluck('id')->each(function ($user_id) use ($faker) {
            for ($i = 0; $i < rand(5, 10); $i++) {
                $post = new Post;
                $post->text = $faker->paragraph;
                $post->access = POST::ACCESS_PUBLIC;
                $post->user_id = $user_id;
                $post->save();

                saveActivityLog(ActivityLog::TYPE_POST, ActivityLog::ACTION_POST_PUBLISH, $post->id, $user_id);

                User::whereNotIn('id', [$user_id])->inRandomOrder()->take(rand(5, 10))->pluck('id')->each(function ($friend_id) use ($post) {
                    $like = savePostLike($post->id, $friend_id, TYPE_POST);
                    saveActivityLog(ActivityLog::TYPE_POST, ActivityLog::ACTION_POST_LIKE, $like->id, $like->user_id);
                });

                User::whereNotIn('id', [$user_id])->inRandomOrder()->take(rand(5, 10))->pluck('id')->each(function ($friend_id) use ($post, $faker) {
                    $comment = new PostComment;
                    $comment->type = TYPE_POST;
                    $comment->user_id = $friend_id;
                    $comment->post_id = $post->id;
                    $comment->text = $faker->words(rand(2, 7), true);
                    $comment->save();
                    saveActivityLog(ActivityLog::TYPE_POST, ActivityLog::ACTION_POST_COMMENT, $comment->id, $comment->user_id);
                });
            }
        });
    }
}
