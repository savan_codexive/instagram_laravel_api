<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\UserFriend;
use Illuminate\Support\Str;

class UserAndFriendSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->username = "savan";
        $user->email = "savan.codexive@gmail.com";
        $user->email_verified_at = now();
        $user->password = bcrypt(12345678);
        $user->remember_token = Str::random(10);
        $user->save();

        User::factory(rand(15, 35))->create();

        User::pluck('id')->each(function ($user_id) {
            User::inRandomOrder()->take(rand(5, 35))->pluck('id')->each(function ($friend_id) use ($user_id) {
                $userFriend = new UserFriend;
                $userFriend->user_id  = $user_id;
                $userFriend->friend_id  = $friend_id;
                $userFriend->save();
            });
        });
    }
}
